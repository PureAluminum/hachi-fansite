/// <reference types="vite/client" />
/// <reference types="youtube" />

declare module '*.vue' {
  import { DefineComponent } from 'vue';
  // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
  const component: DefineComponent<{}, {}, any>;
  export default component;
}

declare module '@/assets/playlist' {
  import SongSet from '@hachi-fansite/common/models/SongSet';
  import StreamSet from '@hachi-fansite/common/models/StreamSet';

  interface Playlist {
    SongSets: SongSet[],
    StreamSet: StreamSet[],
  }
  export default Playlist;
}
