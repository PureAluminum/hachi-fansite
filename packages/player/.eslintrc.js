module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/vue3-recommended',
    'airbnb-base',
    '@vue/typescript/recommended',
  ],
  parserOptions: {
    ecmaVersion: 2020,
    parser: '@typescript-eslint/parser',
  },
  settings: {
    'import/resolver': {
      typescript: {
        alwaysTryTypes: true,
        // monorepo solution: https://github.com/import-js/eslint-plugin-import/issues/1485#issuecomment-830651345
        project: 'packages/player/tsconfig.json',
      },
    },
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    // replace original 'no-shadow' rule with typescript ver
    'no-shadow': 'off',
    '@typescript-eslint/no-shadow': 'error',
    // replace original 'no-unused-expressions' rule with typescript ver
    'no-unused-expressions': 'off',
    '@typescript-eslint/no-unused-expressions': 'error',
    // replace original 'semi' with @typescript-eslint/no-extra-semi
    semi: 'off',
    '@typescript-eslint/semi': 'error',
    // replace original 'indent' with '@typescript-eslint/indent'
    indent: 'off',
    '@typescript-eslint/indent': ['error', 2],
    // prefer-destructuring
    'prefer-destructuring': 'off',
    '@typescript-eslint/no-unused-vars': 'error',
    'import/extensions': [
      'error',
      'ignorePackages',
      {
        js: 'never',
        jsx: 'never',
        ts: 'never',
        tsx: 'never',
      },
    ],
  },
  overrides: [
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
        '**/tests/unit/**/*.spec.{j,t}s?(x)',
      ],
      env: {
        jest: true,
      },
    },
  ],
};
