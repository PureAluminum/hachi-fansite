module.exports = {
  purge: [
    './public/**/*.html',
    './src/**/*.{js,jsx,ts,tsx,vue}',
  ],
  darkMode: 'class', // or 'media' or 'class'
  jit: true,
  theme: {
    extend: {},
  },
  variants: {
    extend: {
    },
  },
  plugins: [
  ],
};
