const plugin = require('tailwindcss/plugin');
const colors = require('tailwindcss/colors');

const rotate = {};
Array.from({ length: 360 }).forEach((_, i) => {
  if (!i) return;
  rotate[i] = `${i}deg`;
  rotate[i * -1] = `${i * -1}deg`;
});

const obj = {};
Array.from({ length: 20 }).forEach((_, i) => {
  if (!i) return;
  Array.from({ length: i + 1 }).forEach(($, x) => {
    if (!x) return;
    obj[`${x}/${i + 1}`] = `${(x / (i + 1)) * 100}%`;
  });
});

module.exports = {
  purge: [
    './index.html',
    './src/**/*.{js,jsx,ts,tsx,vue}',
  ],
  darkMode: 'class', // or 'media' or 'class'
  jit: true,
  theme: {
    extend: {
      width: { ...obj },
      height: { ...obj },
      inset: { ...obj },
      rotate,
    },
  },
  variants: {
    extend: {
    },
  },
  plugins: [
    plugin(({ addComponents }) => {
      const baseName = '.text-outline';
      const textShadow = {
        [baseName]: {
          textShadow: '-1px -1px 0 var(--tw-text-outline-color),\n 1px -1px 0 var(--tw-text-outline-color),\n -1px 1px 0 var(--tw-text-outline-color),\n 1px 1px 0 var(--tw-text-outline-color)',
        },
      };

      const add = (colorCode, colorGroup, number) => {
        const key = !number
          ? `${baseName}-${colorGroup}`
          : `${baseName}-${colorGroup}-${number}`;

        textShadow[key] = {
          '--tw-text-outline-color': colorCode,
        };
      };
      const batchAdd = (colorGroup) => {
        Object.entries(colors[colorGroup])
          .forEach(([number, colorCode]) => add(colorCode, colorGroup, number));
      };

      add(colors.black, 'black');
      add(colors.white, 'white');
      add('transparent', 'transparent');
      add('currentColor', 'currentColor');
      ['gray', 'red', 'yellow', 'green', 'blue', 'indigo', 'purple', 'pink'].forEach(batchAdd);

      addComponents(textShadow);
    }),
  ],
};
