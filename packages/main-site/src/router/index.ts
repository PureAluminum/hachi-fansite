import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';

import defaultView from '@/layout/default.vue';

const routes: RouteRecordRaw[] = [
  {
    path: '',
    component: defaultView,
    children: [
    ],
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
