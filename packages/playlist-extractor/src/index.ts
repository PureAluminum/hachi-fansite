import fs from 'fs';
// eslint-disable-next-line camelcase
import { sheets_v4 } from 'googleapis';
import { URL } from 'url';
import { v4 } from '@lukeed/uuid/secure';

import SongSet from '@hachi-fansite/common/models/SongSet';
import StreamSet from '@hachi-fansite/common/models/StreamSet';

const json = fs.readFileSync('./data.json', 'utf-8');

// eslint-disable-next-line camelcase
const sheetObj = JSON.parse(json) as sheets_v4.Schema$Spreadsheet;

const data = sheetObj?.sheets?.[0]?.data?.[0];

if (!data) process.exit(1);

const result = {
  StreamSets: [] as StreamSet[],
  SongSets: [] as SongSet[],
};
let currentStreamId = '';

data.rowData?.shift(); // remove header
data.rowData?.forEach((x) => {
  const item = new SongSet();

  x.values?.forEach((y, i) => {
    const content = y.userEnteredValue?.stringValue;

    if (i === 0) {
      if (content) {
        const streamSet = new StreamSet();
        streamSet.Id = v4();
        streamSet.Title = content;
        currentStreamId = streamSet.Id;

        result.StreamSets.push(streamSet);
      }

      item.StreamId = currentStreamId;
    }

    if (i === 3 && content) {
      item.SongTitle = content;
      item.SongUrl = y.userEnteredFormat?.textFormat?.link?.uri || '';
      try {
        const url = new URL(item.SongUrl);
        item.StartTime = Number(url.searchParams.get('t')) || undefined;

        const streamSet = result.StreamSets.find((z) => z.Id === currentStreamId);
        if (url.pathname === '/watch') {
          if (streamSet && !streamSet.VideoId) streamSet.VideoId = url.searchParams.get('v') || '';
        } else if (streamSet && !streamSet.VideoId) {
          streamSet.VideoId = url.pathname.substring(1);
        }
      } catch (error) {
        // do nothing
      }
    }

    if (i === 6 && content) {
      item.SongArtist = content;
    }

    if (i === 8 && content) {
      item.Version = content as Extract<SongSet, 'Version'>;
    }
  });

  if (item.SongUrl) result.SongSets.push(item);
});

console.log({ songCount: result.SongSets.length, streamCount: result.StreamSets.length });

fs.writeFileSync('./playlist.json', JSON.stringify(result, null, 2));
